from flask import Flask, render_template, redirect, url_for
app = Flask(__name__)

@app.route('/')
def root():
  return render_template('home.html'), 200

@app.route('/cafe')
def cafe():
  return render_template('cafeall.html'), 200

@app.route('/cafes')
def cafes():
  return redirect(url_for('.cafe'))

@app.route('/cafe_price')
def cafePrice():
  return render_template('cafe_price.html'), 200

@app.route('/cafe_rating')
def cafeRating():
  return render_template('cafe_rating.html'), 200

@app.route('/restaurants')
def restaurants():
  return redirect(url_for('.restaurant'))

@app.route('/restaurant')
def restaurant():
  return render_template('restaurantall.html'), 200

@app.route('/restaurant_price')
def restaurantPrice():
  return render_template('restaurant_price.html'), 200

@app.route('/restaurant_rating')
def restaurantRating():
  return render_template('restaurant_rating.html'), 200

@app.route('/cafe/salt')
def salt():
  return render_template('salt.html'), 200

@app.route ('/cafe/beachhouse')
def beachhouse():
  return render_template('beachhouse.html'), 200

@app.route('/restaurant/thaicaravan')
def thaicaravan():
  return render_template('thaicaravan.html'), 200

@app.route('/cafe/filamentcoffee')
def filamentcofee():
  return render_template('filamentcofee.html'), 200

@app.route('/cafe/projectcoffee')
def projectcoffee():
  return render_template('projectcoffee.html'), 200

@app.route('/restaurant/lafavorita')
def lafavorita():
  return render_template('lafavorita.html'), 200

@app.route('/restaurant/wildmanwood')
def wildmanwood():
  return render_template('wildmanwood.html'), 200

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html'), 404

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)

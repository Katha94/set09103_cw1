readme file for the web app "Edinburgh" created in the module Advanced Webtechnologies set09103 at the Napier University Edinburgh
Made by Katharina Endsuleit
25.10.2016
-----------
How to run the web app?
  -----------
1.) Open the Terminal window of your OS.
Windows:
Navigate to start > search "cmd" (in the little box). Press enter.

Mac:
Navigate to Applications > Utilities > Terminal

Ubuntu and Linu:
By default in Ubuntu and Linux Mint the terminal shortcut key is mapped to Ctrl+Alt+T
  -----------
2.) Navigate to 'src' where the python file 'edinburgh.py' is located. Once you are in the right directory type in the command:
> python edinburgh.py
The console should show the message:
'Running at http://0.0.0.0:5000/'
-----------
3.) Open your browser and browse http://localhost:5000
  ---------